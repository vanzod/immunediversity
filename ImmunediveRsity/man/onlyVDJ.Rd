\name{onlyVDJ}
\alias{onlyVDJ}
\title{
Conserve only the identifiable VDJ segments of a read
}
\description{
This function is used to trim the reads so they only contain the VDJ segments, since the reads are sequenced in the JDV direction the begining of the V segment is used to limit the size of the reads. This position is obtained using the \code{getPositions} function.
}
\usage{
onlyVDJ(reads, database, proc = 1, BLAST = FALSE, bin, param = NA, FASTA, file)
}
\arguments{
  \item{reads}{
A set of reads in FASTA or SFF format.
}
  \item{database}{
Database of the V segments.
}
  \item{proc}{
Number of processors to be used.
}
  \item{BLAST}{
logical. \code{TRUE} if BLAST is to be used and \code{FALSE} if FAAST is to be used.
}
  \item{bin}{
Path to BLAST or FAAST depending on the value selected in \code{BLAST}.
}
  \item{param}{
Parameters to be used in the alignment. Defaults for BLAST are -gapopen 0 -gapextend 4 -penalty -2 -evalue .00000000000000000001.
}
  \item{FASTA}{
The set of reads in FASTA format. In case BLAST is being used this file must be the same as the file specified in \code{reads}, if using an SFF in \code{reads}, here you must provide the associated FASTA.
}
  \item{file}{
Name of the output FASTA file.
}
}
\value{
It returns a ShortRead object which contains the trimmed sequences. If specified it also generates a FASTA file with the trimmed sequences and their associated identifiers.
}
\author{
Carlos Vargas-Chavez
}
\examples{
## It is not possible to include functional examples for this function as it depends on the getPositions function which depends on BLAST or FAAST, two external applications which should be installed by the user.

# Here is an example:
#onlyVDJ("sample.fna", "IGHV.fa", 1, TRUE, '/opt/Bio/ncbi/bin', "sample.fna")
}
