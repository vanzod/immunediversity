\name{getDSegment}
\alias{getDSegment}
\title{
Assign a Dsegment to each read
}
\description{
This function assigns a D segment to a set of reads. Ideally this reads have been stripped of the V and J segments using the getVJsegments function. It uses usearch to perform a search against a D database and also clusterizes the reads, gets the consensus and performs another search using the consensus.
}
\usage{
getDSegment(reads, Ddatabase, bin = "/home/Winter_ImmunediveRsity/", id = 0.8, wordsize = 7, minlength = 10, queryfract = 0.6)
}
\arguments{
  \item{reads}{
A set of reads in FASTA or SFF format.
}
  \item{Ddatabase}{
Database of the D segments in FASTA format.
}
  \item{bin}{
The path to the executable of USEARCH.
}
  \item{id}{
Refers to the minimum identity desired between two elements. It ranges from 0 to 1 meaning 0% to 100% identity.
}
  \item{wordsize}{
The size of the k-mers used to seed the alignments
}
  \item{minlength}{
The minimum size of a read to be considered.
}
  \item{queryfract}{
Smallest fraction of the query required to be covered in the alignment, it ignores gaps. It ranges from 0 to 1 meaning 0% to 100% coverage.
}
}
\value{
Returns a data.frame with 3 columns, the 1st contains the IDs of the reads, the 2nd the identified D segment according to the consensus search, the 3rd contains the identified D segment after performing an individual search.
}
\author{
Carlos Vargas-Chavez
}
\examples{
## It is not possible to include functional examples for this function as it depends on the usearchaln and usearchclust functions which depend on USEARCH, an external application which should be installed by the user.

# Here is an example:
#getDSegment("VJ.fa", "IGHD.fa")
}
