\name{usearchclust}
\alias{usearchclust}
\title{
Clusterize a set of reads using USEARCH
}
\description{
This function is used to clusterize a set of reads using usearch. The output are two files: the output file containing all the clusters and the reads belonging to each cluster and a FASTA file which contains the consensus sequence for each cluster.
}
\usage{
usearchclust(reads, bin = "/home/Winter_ImmunediveRsity", id = 0.9, wordsize = 5, minlength = 10, local = FALSE, queryfract = 0.7, outfile, sort = FALSE)
}
\arguments{
  \item{reads}{
A set of reads in FASTA format.
}
  \item{bin}{
The path to the executable of USEARCH.
}
  \item{id}{
Refers to the minimum identity desired between two elements. It ranges from 0 to 1 meaning 0% to 100% identity.
}
  \item{wordsize}{
The size of the k-mers used to seed the alignments
}
  \item{minlength}{
The minimum size of a read to be considered.
}
  \item{local}{
logical. \code{TRUE} if you wish a local alignment and \code{FALSE} if you require a global alignment.
}
  \item{queryfract}{
Smallest fraction of the query required to be covered in the alignment, it ignores gaps. It ranges from 0 to 1 meaning 0% to 100% coverage.
}
  \item{outfile}{
Title of the output file.
}
  \item{sort}{
Whether the reads should be sorted prior to clustering or not.
}
}
\value{
The function returns \code{TRUE} if execution was successful. Additionally it creates two files, one specifying the clusters and another with the consensus of each cluster in a FASTA filed named prepending cons. to the original file name.
}
\references{
Edgar,R.C. (2010), Search and clustering orders of magnitude faster than BLAST, Bioinformatics.
doi: 10.1093/bioinformatics/btq461
}
\author{
Carlos Vargas-Chavez
}
\note{
This function requires a functional installation of USEARCH (at least 5.0) in your UNIX system. You can download the latest USEARCH here http://www.drive5.com/usearch/nonprofit_form.html
}
\seealso{
http://www.drive5.com/usearch/
}
\examples{
## It is not possible to include functional examples for this function as it depends on USEARCH, an external application which should be installed by the user.

# Here is an example:
# usearchclust("trimmed.fa", '~', id = 0.9, wordsize = 5, minlength = 10, local = FALSE, queryfract = 0.7, "clust.uc", FALSE)
}
