#  idiotypesAndAlleles.R
#  
#  Copyright 2013-2014 
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  


idiotypesAndAlleles <- function(alleles="Alleles_consensusfullVDJ.txt") {
    alleles <- read.delim(alleles, header=TRUE)
    idiotypesAndAlleles<-vector(length=nrow(alleles))

    for (x in c(1:nrow(alleles))) {
        idiotype<-strsplit(as.character(alleles$read[x]), "[.]")[[1]]
        idiotypesAndAlleles[x] <- paste(idiotype[1], alleles$V[x], alleles$D[x],
                                        alleles$J[x], idiotype[4], idiotype[5],
                                        sep=".")
	}
    
    write.table(idiotypesAndAlleles, file="VDJ_alleles_idiotype.txt",
                row.names=FALSE, col.names=FALSE, quote=FALSE)
}
