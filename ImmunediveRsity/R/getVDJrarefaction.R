#  getVDJrarefaction.R
#  
#  Copyright 2013-2014 
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  


getVDJrarefaction <- function(VDJinput, getTable=FALSE, file=NULL, IN_vector=TRUE,
                              clone_group=TRUE, maintitle="VDJ Rarefaction Curve") {
    # This function takes a  vector with the V D J assingation and generates a
    # rarefaction
    # If IN_vector is set to TRUE verifies that the input received is a table
    # otherwise the input is processed as a table
        
    if (IN_vector) {
        if (is.vector(VDJinput)) {
            idiotype <- VDJinput
        } else {
            stop("The VDJinput is not a vector and the IN_vector parameter is TRUE")
        }
    } else {
        # This module Verifies which VDJ weren't classified and changes the
        # NA value for "Unclassified"
        idiotype <- apply(VDJinput, 1, function(input){  
            index <- grep(TRUE, is.na(input), value=FALSE)
            input[index] <- "Unclassified"
            index <- grep(" ", input, value=FALSE)
            input[index] <- "Unclassified"
            paste(input, collapse="|")
        }) 
    }
        
    # this module generate the subsamples based on the number of reads that we have
    # and calculates the rarefaction value of the samples 
    steps <- round(log(length(idiotype), base=10))
        
    if (steps < 3) {
        steps <- as.numeric(paste("1", paste(rep(0, times=steps - 1), collapse=""),
                                  sep="", collapse=""))
    } else {
        steps <- as.numeric(paste("1", paste(rep(0, times=steps - 2), collapse=""),
                                  sep="", collapse=""))
    }
        
    samplesize <- steps
    total_groups <- t(table(idiotype))
    rarefaction <- NULL

    for (i in 1:length(idiotype)) {
        if (samplesize > length(idiotype)) {
            break
        }
            
        #We use the rarefy function from CRAN's vegan package
        rarefaction <- rbind(rarefaction, c(samplesize, rarefy(total_groups,
                                                               samplesize)[1]))
        samplesize <- samplesize + steps
        i <- samplesize
    }
        
    if (getTable) {
        colnames(rarefaction) <- c("Reads sampled", "No. of heavy VDJ combinations captured")
        return(rarefaction)
    } else {
        if(!is.null(file)) {
            postscript(file=file, horizontal=FALSE, onefile=FALSE,
                       paper="special", width=7, height=7)
        }
            
        if(clone_group){
            plot(rarefaction[,1],rarefaction[,2], ylab="No. of heavy chain clonotypes captured",
                 xlab="Reads sampled", type="o", col="blue", main=maintitle)
        }else {
            plot(rarefaction[,1],rarefaction[,2], ylab="No. of unique heavy chains captured",
                 xlab="Reads sampled", type="o", col="blue", main=maintitle)
        }
        if(!is.null(file)){
            dev.off()
        }
    }
}
