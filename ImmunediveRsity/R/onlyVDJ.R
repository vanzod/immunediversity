#  onlyVDJ.R
#  
#  Copyright 2013-2014 
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  


onlyVDJ <- function(FASTQ = NA, database, proc = 1, BLAST = TRUE, bin, param = NA, file, matchedVsfile = "matchedVsfile.txt"){
                                        #  This function is used to trim the reads so they only contain the VDJ segments, since the reads are sequenced in the JDV direction
                                        #  the begining of the V segment is used to limit the size of the reads. This position is obtained using the getPositions function.
  #PARAMS:
  #
  #    database.  Vdatabase, /opt/ImmunediveRsity/data/IGBLAST/database/human_gl_V
  #    bin.   Path to blast bin. /opt/ImmunediveRsity/bin/blast-2.2.22/bin
  #    FASTQ. Fastq file  
  
  if(!is.na(FASTQ)){
    if(!(file.exists(FASTQ)))
      stop("File specified in 'FASTQ' does not exist.")
  }
  if(file.exists(matchedVsfile)){
      stop("'matchedVsfile' must be a non existing file, delete it or set a new file name.")
  }

  reads_temp <- readFastq(FASTQ)
  writeFasta(reads_temp, 'Temp_file_reads.fna')
  reads <- 'Temp_file_reads.fna'
  #system('rm Temp_file_reads.fna', wait)

  if(BLAST == TRUE){
    if(is.na(param)){
      #  old params. -G 0 -E 4 -q -2 -e .00000000000000000001
      param <- "-G 0 -E -1 -q -2 -e .00000000000000000001"
    }
  }
  
  table <- getPositions(reads, database, proc, bin, param)

  print('Recortando...')
  reads <- readFastq(FASTQ)

  # esto es lo que da problemas
  new <- ShortReadQ(sread = subseq(sread(reads), start = 1, end = table[,3]), 
                    quality = new(Class= class (quality(reads)), 
                    quality=subseq(quality(quality(reads)), start = 1, end = table[,3])), id = id(reads))
  
  writeFastq(new, file = file)

  if(!is.na(matchedVsfile)){
    write.table(table[, 4], matchedVsfile, quote = FALSE, row.names = FALSE, col.names = FALSE)
  }

  # return variables to null value
  reads <- NULL 
  reads_temp <- NULL
  table <- NULL

  gc()

  #return(new)
}

