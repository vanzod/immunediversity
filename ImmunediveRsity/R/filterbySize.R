#  filterbySize.R
#  
#  Copyright 2013-2014 
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  


filterbySize <-
    function(reads, minsize=200, file=NULL){
        # This function removes reads shorter than a specified value.
        # It returns a ShortRead object including only the reads larger
        # than the specified size, and if desired it can also write the output
        # in a specified FASTQ file.
        if (!(is(reads, "ShortReadQ"))) {
            if(!is(reads, "ShortRead"))
                stop("'reads' must be a ShortReadQ or ShortRead object, try generating it using the read454 or readFasta function.")
        }
        
	if (!(is.numeric(minsize)))
            stop("'minsize' must be a number specifying the smallest desired size.")
        
        if (!is.null(file)) {
            if ((file.exists(file)))
                stop("'file' must be a non existing file, delete it or set a new file name.")
        }
        
        reads <- reads[width(reads) >= minsize]

        if(!is.null(file))
            writeFastq(reads, file=file)
        
        return(reads)
    }
