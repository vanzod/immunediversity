#  getVJSegments.R
#  
#  Copyright 2013-214
#
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#


getVJSegments <-
function(reads, Vdatabase, Jdatabase, proc = 1, BLAST = FALSE, bin, Vparam = NA, Jparam = NA, FASTA, file){
    #This function is used to align the reads against the V segments database and return the positions on the read. Next it generates a FASTA file trimming the reads removing the V segments and performs another search but now against the J segments database. It can use either BLAST or FAAST to align. In case it uses BLAST it requires a FASTA file, in case it uses FAAST it can receive a .sff, a FASTA or a FFASTA file for the alignment. When using an .sff or a FFASTA they are only used to determine the coordinates, therefore you must also specify the FASTA related to the file in order to trim the reads.
    if(!(file.exists(Vdatabase)))
        stop("File specified in 'Vdatabase' does not exist.")
    if(!(file.exists(Jdatabase)))
        stop("File specified in 'Jdatabase' does not exist.")
    if(!is.null(file))
        if((file.exists(file)))
            stop("'file' must be a non existing file, delete it or set a new file name.")
	if(BLAST == TRUE){
		if(is.na(Vparam))
			Vparam <- "-gapopen 0 -gapextend 4 -penalty -2 -evalue .00000000000000000001"
		if(is.na(Jparam))
			Jparam <- "-gapopen 4 -gapextend 2 -penalty -2 -word_size 13"
	}
    print("Aligning V segments")
    #Using the getPositions function the reads are aligned against the V segments database. It creates a temporary FASTA file named tempfasta.fa which contains the reads without the V segment.
    Vpositions <- getPositions(reads, Vdatabase, proc, BLAST, bin, Vparam, FASTA)
	fasta <- readFasta(FASTA)	
	new <- ShortRead(sread = subseq(sread(fasta), start = 1, end = Vpositions[,2]-1), id = id(fasta))
	writeFasta(new, file = "tempfasta.fa")
	DJFASTA <- "tempfasta.fa"
	if(BLAST==TRUE){
		reads <- DJFASTA
    } else
    #temporalmente reads tambien es FASTA ya que aun no permite cortar .sff, para estar totalmente completo aqui se debe permitir el corte del objeto reads para usarlo para alinear, reads <- cortar el .sff con sfftools
    #reads <- "tempreads.sff"
		reads <- DJFASTA
    print("Aligning J segments")
    #Using the getPositions function the reads are aligned against the J segments database. It uses the file named temp.fasta.fa
    Jpositions <- getPositions(reads, Jdatabase, proc, BLAST, bin, param = Jparam, DJFASTA)
    new <- ShortRead(sread = subseq(sread(fasta), start = Jpositions[,3]+1, end = Vpositions[,2]-1), id = id(fasta))
    system("rm tempfasta.fa", wait = TRUE)
    new <- new[width(new)>1]
    #Writes a FASTA file with the remaining portion of the read. In case no V or J segments were identified the whole read is returned.
    writeFasta(new,file=file)
    VJ <- data.frame(Vpositions[,1], Vpositions[,4], Vpositions[,2:3], Jpositions[,4], Jpositions[,2:3])
    colnames(VJ) <- c("read", "V", "Vleft", "Vright", "J", "Jleft", "Jright")
    return(VJ)
}