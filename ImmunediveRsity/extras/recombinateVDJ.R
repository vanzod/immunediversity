#  recombinateVDJ.R
#  
#  Copyright 2013-214
#
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#


############################################################################################
#FUNCTION: recombinateVDJ                                                                  #
#This function generates a VDJ recombinated read  from the given V, D, J, gene.            #
#M                                                                                         #
############################################################################################
recombinateVDJ <-
function(Vgene, Dgene, Jgene, N1prob=0.8, N2prob=0.8, Vdelprob=0.02, Jdelprob=0.02, Nonframeprob=0.118, main_allele=FALSE){

N1val<-sample(c(TRUE,FALSE), size=1, prob=c(N1prob, 1-N1prob))
N2val<-sample(c(TRUE,FALSE), size=1, prob=c(N2prob, 1-N2prob))
Vdel<-sample(c(TRUE,FALSE), size=1, prob=c(Vdelprob, 1-Vdelprob))
Jdel<-sample(c(TRUE,FALSE), size=1, prob=c(Jdelprob, 1-Jdelprob))
Nonframe<-sample(c(TRUE,FALSE), size=1, prob=c(Nonframeprob, 1-Nonframeprob))
N1seq<-NULL
N2seq<-NULL
sequences<-NULL
Vseq<-NULL
Jseq<-NULL
#First we verify if it is going to be the main allele or not. If it is, the resulting allele should be an in-frame sequence
if(main_allele){
#We create the N1val fragment if N1val is true with N1valprobability
	if(N1val){
		while(N1val){
			N1seq<-paste(sample(c("A","C","T","G"), sample(c(3,6,9,12), size=1), replace=TRUE), collapse="")
			if((grepl("TGA",N1seq)|grepl("TAG",N1seq)|grepl("TAA",N1seq))){
			} else { N1val<-FALSE }
		}
	}
#We create the N2val fragment if N2val is true with N2valprobability
	if(N2val){
		while(N2val){
			N2seq<-paste(sample(c("A","C","T","G"), sample(c(3,6,9,12), size=1), replace=TRUE), collapse="")
			if(grepl("TGA",N2seq)|grepl("TAG",N2seq)|grepl("TAA",N2seq)){
			} else { N2val<-FALSE }
		}
	}
#we set the Vgene
	if(Vdel){
		Vseq<-substr(Vgene, start=1,stop=(nchar(Vgene)-(nchar(Vgene)%%3)))
	} else { Vseq<-Vgene }
#we set the Jgene
	if(Jdel){
		Jseq<-substr(Jgene, start=1+(nchar(Jgene)%%3),stop=nchar(Jgene))
	} else { Jseq<-Jgene}
	
	if((sum(nchar(Vseq), nchar(Dgene), nchar(Jseq), nchar(N1seq), nchar(N2seq))%%3)==0 ){
		return(paste(Vseq, N1seq, Dgene, N2seq, Jseq, sep="", collapse=""))
	} else {
		return(paste(Vseq, N1seq, Dgene, N2seq, paste(Jseq, paste(sample(c("A","C","T","G"), size=(sum(nchar(Vseq), nchar(Dgene), nchar(Jseq), nchar(N1seq), nchar(N2seq))%%3), replace=TRUE), sep="", collapse=""), sep="", collapse=""), sep="", collapse=""))
	}

} else{
	#We create the N1val fragment if N1val is true with N1valprobability
	if(N1val){
		while(N1val){
			N1seq<-paste(sample(c("A","C","T","G"), sample(c(1:12), size=1), replace=TRUE), collapse="")
			if(grepl("TGA",N1seq)|grepl("TAG",N1seq)|grepl("TAA",N1seq)){
			} else { N1val<-FALSE }
		}
	}
#We create the N2val fragment if N2val is true with N2valprobability
	if(N2val){
		while(N2val){
			N2seq<-paste(sample(c("A","C","T","G"), sample(c(1:12), size=1), replace=TRUE), collapse="")
			if(grepl("TGA",N2seq)|grepl("TAG",N2seq)|grepl("TAA",N2seq)){
			} else { N2val<-FALSE }
		}
	}
#we set the Vgene
	if(Vdel){
		Vseq<-substr(Vgene, start=1,stop=(nchar(Vgene)-(nchar(Vgene)%%3)))
	} else { Vseq<-Vgene }
#we set the Jgene
	if(Jdel){
		Jseq<-substr(Jgene, start=1+(nchar(Jgene)%%3),stop=nchar(Jgene))
	} else { Jseq<-Jgene}
	
	if(!Nonframeprob){
		return(paste(Vseq, N1seq, Dgene, N2seq, paste(Jseq, paste(sample(c("A","C","T","G"), size=(sum(nchar(Vseq), nchar(Dgene), nchar(Jseq), nchar(N1seq), nchar(N2seq))%%3), replace=TRUE), sep="", collapse=""), sep="", collapse=""), sep="", collapse=""))
	} else {
		if((sum(nchar(Vgene), nchar(Dgene), nchar(Jgene), nchar(N1seq), nchar(N2seq))%%3)==0){
			return(paste(Vseq, N1seq, Dgene, N2seq, paste(Jseq, paste(sample(c("A","C","T","G"), size=1, replace=TRUE), sep="", collapse=""), sep="", collapse=""), sep="", collapse=""))
		} else{ return(paste(Vseq, N1seq, Dgene, N2seq, Jseq, sep="", collapse="")) }
	}

}
}