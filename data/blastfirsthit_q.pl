#!/usr/bin/env perl

#  blastfirsthit_q.pl
#  
#  Copyright 2013
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
# 


##################################
# This program select one Blast hit per query, the one with the highest score.
# 
# Run:
# cat output.blast | perl blastfirsthit_q.pl > output_blastfirtshit.blast

$lastquery="NA";
$lastsubject="NA";
$lasthitlength=0;
while(<STDIN>){
        $line=$_;
        chomp($line);
        @hit=split(/\t/, $line);
        $query=$hit[0];
        $subject=$hit[1];
        $hitlength=$hit[3];
        if($query ne $lastquery){
                print "$line\n";
                $lastsubject=$subject;
                $lastquery=$query;
        }
}

