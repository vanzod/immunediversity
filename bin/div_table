#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  div_table
#  
#  Copyright 2013-2014 andresyoshimar@gmail.com
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
# 


__author__  = 'Andres Aguilar'
__email__   = 'andresyoshimar@gmail.com'
__date__    = '07/03/2014'
__version__ = '0.0.2'

"""
############################################################################################
# description                                                                              #
#                                                                                          #
# How to use:                                                                              #
#    ./tabla.py -g groupsfullVJ.txt [-o archivo_salida.tab]                                #
#     python tabla.py -g groupsfullVJ.txt [-o archivo_salida.tab]                          #
############################################################################################
"""

import math
import argparse
from os import path, mkdir, popen
from shutil import rmtree
from math import log

dir_name = 'cg_temp'


def make_file(nombre, datos):
    archivo = open(dir_name + '/' + nombre, 'a')
    archivo.write(datos + '\n')
    archivo.close()

        
def divide_groups(id_list, freq_list):
    """ Function to divide each clonal group """

    if (len(id_list) != len(freq_list)):
        # if lens aren't equals break
        return 0

    clonal_groups = list()
        
    for i in range(0, len(id_list)):
        lista = id_list[i].strip().split('\t')
        
        grupo_temp = lista[0]
        
        grupo = grupo_temp.split('.')
        grupo = grupo[0] + '.' + grupo[1] + '.' + grupo[2] + '.' +grupo[3]

        make_file(grupo, str(id_list[i]) + '\t' + str(freq_list[i]))
        clonal_groups.append(grupo)

    groups = list(set(clonal_groups))
    return groups


def read_file(name):
    """ Read a groupsfullVJ.txt file """

    infile = open(name, 'r')
    lines = infile.read()
    infile.close()

    mylist = lines.split('\n')
    mylist = mylist[:-1]  # delete blank space, last line

    total = len(mylist)
    
    lst = list(set(mylist))
    lst.sort()

    freq = list()  # list of freqs

    # get frequency for each idiotype
    for i in lst:
        frequency = mylist.count(i)
        freq.append(frequency)

    return lst, freq, total


def get_clonal_group(name):
    cg_file = open(dir_name + '/' + name, 'r')
    cg = cg_file.read()
    cg_file.close

    return cg


def shannon_ponderado(lista_individuos):
    """ Calcular shannon ponderado 
    ((N * log(n)) - sum(fi*log(fi)))/N
    
    N  = Suma de todas las frecuencias
    fi = frecuencia de cada idiotipo
    """
    
    N = 0
    for i in lista_individuos:
        N = N + i
    
    N2 = N * math.log(N,10)
    Efi = 0

    for i in lista_individuos:
        temp = i * math.log(i,10)
        Efi = Efi + temp

    return ((N2 - Efi) / N)

    
def shannon_var(lista_individuos):
    """ Varianza del indice ponderado de shannon
    (sum(fi * log(fi)^2) - (sum(fi * log(fi))^2)/N ) / N^2
    """

    N = 0
    for i in lista_individuos:
        N = N + i

    Efi1 = 0

    for i in lista_individuos:
        temp = i * (math.log(i,10) ** 2)
        Efi1 = Efi1 + temp

    Efi2 = 0
    for i in lista_individuos:
        temp = i * math.log(i, 10)
        Efi2 = Efi2 + temp

    Efi2 = Efi2 ** 2

    return ((Efi1 - (Efi2/N))/(N**2))


def gini_index(x): #follow transformed formula
    """Return computed Gini coefficient.

    :note: follows tranformed formula, like R code in 'ineq'
    :see: `gini_index`
    :contact: aisaac AT american.edu
    """
    x = sorted(x)  # increasing order
    n = len(x)
    G = sum(xi * (i+1) for i,xi in enumerate(x))
    G = 2.0*G / (n*sum(x))  # 2*B
    return G - 1 - (1./n)


def shannon_index(lista):
    stList = lista
    list_a = list(set(stList)) # list of symbols in the string

    freqList = []
    for symbol in list_a:
        ctr = 0
        for sym in stList:
            if sym == symbol:
                ctr += 1
        freqList.append(float(ctr) / len(stList))  # normalize

    # Shannon 
    ent = 0.0
    for freq in freqList:
        ent = ent + freq * math.log(freq, 2)

    if ent != 0:
        ent = -ent

    return ent


def calulate_indices(inlist, total, output_file):
    outfile = open(output_file, 'w')

    outfile.write('CG_ID\tNo_of_id\tNo_of_reads\treads_norm\tShannon\tShannon_pon\tShannon_Var\tGini\n')
    for i in inlist:
        cg = get_clonal_group(i)

        cg2 = cg.split('\n')
        cg2 = cg2[:-1]

        freqs = list()
        suma = 0
        
        for j in cg2:
            j2 = j.split('\t')
            freqs.append(int(j2[1]))
            suma = suma + int(j2[1])

        gc_norm = (float(suma) * 100) / total
            
        outfile.write(str(i) + '\t' + str(len(freqs)) + '\t' + str(suma) +'\t'+ str(gc_norm) + '\t' + str(shannon_index(freqs)) +'\t'+ str(shannon_ponderado(freqs)) + '\t' + str(shannon_var(freqs)) +'\t'+ str(gini_index(freqs)) + '\n')

    outfile.close()

        

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='div_table', epilog="div_table is part of ImmunediveRsity and it is GNU software")

    parser.add_argument('-g', '--groupsfullVJ', required=True, help='groupsfullVJ created by ImmunediveRsity')
    parser.add_argument('-o', '--output', default='indices_table.txt', help='output file name. Default: indices_table.txt')

    args = parser.parse_args()

    lista, freqs, total = read_file(args.groupsfullVJ)

    if path.isdir(dir_name):
        rmtree(dir_name)
        mkdir(dir_name)
    else:
        mkdir(dir_name)

    if path.isfile(args.output):
        popen('rm ' + args.output)
    
    groups = divide_groups(lista, freqs)
    calulate_indices(groups, total, args.output)

    if path.isdir(dir_name):
        rmtree(dir_name)

