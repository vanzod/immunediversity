#!/usr/bin/env perl

#  cutVDJ.pl
#  
#  Copyright 2013-2014
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
# 

#use strict;
use warnings;
use Getopt::Long;

#my $author  = 'Andres Aguilar';
#my $date    = '22-03-2014';
#my $email   = 'andresyoshimar@gmail.com';
my $version = '0.2.1';

my $global_counter = 0;
my @starts = ();
my @ends = ();

sub version_message(){
    print("\ncutVDJ\t version $version\n\n");
    print("cutVDJ is part of ImmunediveRsity and it's GNU software\n");
    exit 0;
}

sub help_message(){
    print("cutVDJ $version\n\n");
    print("  -f/-fastq \t\t: Fastq file. (without extension)\n");
    print("  -c/-coordinates \t: txt file with the start and end coordinates. (without extension)\n\n");
    print("cutVDJ is part of ImmunediveRsity and it's GNU software\n");
    exit 0;
}

# read fastq file
sub cut_fastq{
    my @list = @_;

    $counter = 1;

    open FILE, "<", $list[0].'.fastq' or die $!;
    while(my $line = <FILE>){
	if($counter == 1){
	    # Sequence ID
	    print($line);
	}
	if($counter == 2){
	    # Sequence
	    my $line2 = '';

	    my $start = length($line)-$ends[$global_counter];

	    if($ends[$global_counter] == 0){
		$line2 = substr($line, $start-1);
		print($line2);
	    } else {
		my $end = length($line)-$starts[$global_counter]-$start;

		$line2 = substr($line, $start-1, $end+1);
		print($line2, "\n");
	    }
	}
	if($counter == 3){
	    # Quality ID
	    print($line);
	}
	if($counter == 4){
	    # Quality seq
	    my $line2 = '';

	    my $start = length($line)-$ends[$global_counter];
	    
	    if($ends[$global_counter] == 0){
		$line2 = substr($line, $start-1);
		print($line2);
	    } else {
		my $end = length($line)-$starts[$global_counter]-$start;

		$line2 = substr($line, $start-1, $end+1);
		print($line2, "\n");
	    }
	}
	$counter ++;
	if($counter > 4){
	    $counter = 1;
	    $global_counter ++;
	}
    }
}

sub read_positions{
    my @list = @_;

    open FILE, "<", $list[0].'.tab' or die $!;
    my @lines = <FILE>;

    foreach my $line (@lines){
	my @lista = split(' ', $line);
	
	# append start
	push(@starts, $lista[0]);

	# append end
	push(@ends, $lista[1]);
    }
}

# get params
my %opts = ();
GetOptions( \%opts,
            'f|fastq=s',
            'c|coordinates=s',
	    'v|version',
	    'h|help'
    ) or help_message();

# verify params
my @claves = keys %opts; 
if($#claves < 0){
    # No params, show help message and exit
    help_message();
}

if($opts{'v'}){ version_message(); }
if($opts{'h'}){ help_message(); }

if($opts{'c'} eq '' and $opts{'f'} eq ''){ 
    print("You should specify filenames\n"); 
    help_message();
} else { 
    # call read_positions to get the coordinates
    read_positions($opts{'c'});

    # call cut_fastq to cut reads
    cut_fastq($opts{'f'});
}
