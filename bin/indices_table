#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  complete_table
#  
#  Copyright 2013-2014
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
# 


import os
import sys
import time
import math
import shutil
import random
import logging
import argparse
import operator
import itertools
import pandas as pd

__author__  = 'Andres Aguilar'
__email__   = 'andresyoshimar@gmail.com'
__version__ = '0.3.3'
__build__   = '2013/12/06'

"""
###########################################################################
# Calcula las reads por grupo clonal y las normaliza, tambien la cantidad #
# de idiotipos por grupo clonal, los indices de Gini y Shannon; así como, #
# la cantidad de mutaciones sinonimas y no sinonimas del idiotipo         #
# mayoritario de cada grupo clonal                                        #
#                                                                         #
# How to use since ipython:                                               #
#   complete_table('groupsfullVJ.txt', 'file-final.txt', 'output.txt')    #
#                                                                         #
# How to use since a shell:                                               #
#   ./complete_table -g groupsfullVJ.txt -f file-final.txt -o outfile.txt #
###########################################################################
"""

dir_name = 'mutaciones_temp'


def find_element(lista, el):
    """ Return the element index in a list 
    """
    for i in range(len(lista)):
        if lista[i] == el:
            return i


def get_maximum(nombre):
    """ return KaKs from the most representative idiotype 
    """
    nombres = list()
    mut_s   = list()
    mut_ns  = list()
    freq    = list()

    archivo = open(dir_name + '/' + nombre, 'r')
    
    while True:
        linea = archivo.readline()
        if not linea:
            break
        
        lista = linea.strip().split('\t')

        nombres.append(lista[0])
        mut_s.append(lista[1])
        mut_ns.append(lista[2])
        freq.append(int(lista[3]))

    indice = find_element(freq, max(freq))
    result = (mut_s[indice], mut_ns[indice], nombres[indice])
    
    return result
    

def make_file(nombre, datos):
    archivo = open(dir_name + '/' + nombre, 'a')
    archivo.write(datos)
    archivo.close()


def divide_groups(input_file):
    """ Function to divide each clonal group """
    
    archivo = open(input_file, 'r')
    grupos = list()
    while True:
        linea = archivo.readline()
        if not linea:
            break  # EOF

        lista = linea.strip().split('\t')
        grupo_temp = lista[0]
        
        grupo = grupo_temp.split('.')
        grupo = grupo[0] + '.' + grupo[1] + '.' + grupo[2] + '.' +grupo[3]

        make_file(grupo, linea)
        grupos.append(grupo)

    archivo.close()


def make_temp_file(file):
    """ Function to create the temp file """

    nombre_archivo = file[:-4]
    
    archivo = open(file, 'r')
    salida  = open(nombre_archivo + '.temp', 'w')

    while True:
        linea = archivo.readline()

        if not linea:
            break  # verify EOF

        lista = linea.strip().split('\t')
        if len(lista) == 4:
            salida.write(linea)
    
    archivo.close()
    salida.close()
    
    res = nombre_archivo+'.temp'
    return res


def shannon_index(lista):
    stList = lista
    list_a = list(set(stList)) # list of symbols in the string

    freqList = []
    for symbol in list_a:
        ctr = 0
        for sym in stList:
            if sym == symbol:
                ctr += 1
        freqList.append(float(ctr) / len(stList))  # normalize

    # Shannon 
    ent = 0.0
    for freq in freqList:
        ent = ent + freq * math.log(freq, 2)
    ent = -ent

    return ent

def shannon_ponderado(lista_individuos):
    """ Calcular shannon ponderado 
    ((N * log(n)) - sum(fi*log(fi)))/N
    
    N  = Suma de todas las frecuencias
    fi = frecuencia de cada idiotipo
    """
    
    N = 0
    for i in lista_individuos:
        N = N + i
    
    N2 = N * math.log(N,10)
    Efi = 0

    for i in lista_individuos:
        temp = i * math.log(i,10)
        Efi = Efi + temp

    return ((N2 - Efi) / N)

    
def shannon_var(lista_individuos):
    """ Varianza del indice ponderado de shannon
    (sum(fi * log(fi)^2) - (sum(fi * log(fi))^2)/N ) / N^2
    """

    N = 0
    for i in lista_individuos:
        N = N + i

    Efi1 = 0

    for i in lista_individuos:
        temp = i * (math.log(i,10) ** 2)
        Efi1 = Efi1 + temp

    Efi2 = 0
    for i in lista_individuos:
        temp = i * math.log(i, 10)
        Efi2 = Efi2 + temp

    Efi2 = Efi2 ** 2

    return ((Efi1 - (Efi2/N))/(N**2))


def calc_gini2(x): #follow transformed formula
    """Return computed Gini coefficient.

    :note: follows tranformed formula, like R code in 'ineq'
    :see: `calc_gini`
    :contact: aisaac AT american.edu
    """
    x = sorted(x)  # increasing order
    n = len(x)
    G = sum(xi * (i+1) for i,xi in enumerate(x))
    G = 2.0*G/(n*sum(x)) #2*B
    return G - 1 - (1./n)


def get_indices(input_file):
    """ Crea un archivo con las frecuencias de idiotipos 
    por grupo clonal y sirve como lanzador para la funcion
    que calcula el indice de gini o de shannon
    
    :use: get_indices('input_file_neme-final.txt')
    """
    archivo = open(input_file, 'r')
    salida = open(input_file[:-4] + '.tmp', 'w')

    while True:
        linea = archivo.readline()
        if not linea: 
            break  # Verify EOF

        lista = linea.strip().split('\t')
        
        if len(lista) == 1:  # is a Clonal group
            salida.write('\n' + lista[0])
        if len(lista) == 4:  # is an Idiotype
            salida.write('\t' + lista[-1])
        
    archivo.close()
    salida.close()

    # Read file to calculate indices (Shannon end Gini)

    archivo = open(input_file[:-4] + '.tmp', 'r')
    
    primera = 0  # First line. 0 yes - 1 not

    salida_s = list()
    salida_s2 = list()
    salida_g = list()
    salida_sp = list()
    salida_sv = list()

    while True:
        linea = archivo.readline()
        
        if not linea:
            break  # Verify EOF

        if primera == 0:
            primera = 1
            continue

        lista = linea.strip().split('\t')
        arreglo = lista[1:]
        arreglo = [int(el) for el in arreglo]

        # SHANNON index

        # Sum of Idiotype frequenciesto normalize
        suma = 0
        for el in arreglo:
            suma = suma + el

        s_index2 = shannon_index(arreglo)

        div = 0
        if (math.log(suma) == 0.0):
            print("Avoid zero division!")
            div = 0.000000000000001
        else:
            div = math.log(suma)

        s_index = shannon_index(arreglo)/div
        s_ponderado = shannon_ponderado(arreglo)
        s_var = shannon_var(arreglo)

        salida_s2.append(str(s_index2))
        salida_s.append(str(s_index))
        salida_sp.append(str(s_ponderado))
        salida_sv.append(str(s_var))

        # GINI index
        salida_g.append(str(calc_gini2(arreglo)))

    archivo.close()
    
    # Delete all temp files
    os.popen('rm ' + input_file[:-4] + '.tmp')
    
    return salida_s, salida_s2, salida_g, salida_sp, salida_sv


def get_frequency(file):
    """ Saca la frecuencia de repeticion de lineas
    en un archivo
    
    :use: get_frequency('groupsfullVJ.txt')
    """
    archivo = open(file, 'r')

    lineas = archivo.read()
    lista = lineas.split('\n')
    lista = lista[:-1]
    lista.sort()

    # Count elements in the list
    # It's the no. of reads in the library
    tam = len(lista)

    lista_temp = list()
    str_res = ''

    for el in lista:
        if el not in lista_temp:
            lista_temp.append(el)
            str_res = str_res + str(el) + '\t' + str(lista.count(el)) + '\n'

    archivo.close
    return str_res, tam


def complete_table(input_file, input_file_final, output_file):
    """ Function to make a table with:
    Clonal group identifier
    Idiotypes per clonal group
    Reads per clonal group
    Normalized reads per clonal group
    Gini index
    Shannon index
    Normalized shannon index
    Ks from the most representative idiotype
    Ka from the most representative idiotype
    Identifier for the most representative idiotype (with the highest frequency)

    :use: complete_table('groupsfullVJ.txt', 'file-final.txt', 'outputfile.txt')
    """
    
    # reads per Clonal Group
    os.popen("cut -d'.' -f1-4 " + input_file + " > " + input_file[:-4] + "1.txt")
    
    # Idiotypes per Clonal Groups
    os.popen("cat " + input_file + " |sort|uniq|cut -d'.' -f1-4 > " + input_file[:-4] + "2.txt")

    # Wait for process
    time.sleep(5)

    # Frequency of each file (idiotypes and clonal groups)
    reads_gc, tam_idiot_gc = get_frequency(input_file[:-4] + "1.txt")
    idiot_gc, tam_reads_gc = get_frequency(input_file[:-4] + "2.txt")

    # delete freq temp files
    os.popen('rm ' + input_file[:-4] + "1.txt")
    os.popen('rm ' + input_file[:-4] + "2.txt")

    # split by new line character
    lista_reads_gc = reads_gc.split('\n')
    lista_idiot_gc = idiot_gc.split('\n')

    # Delete last element (empty element)
    lista_reads_gc = lista_reads_gc[:-1]
    lista_idiot_gc = lista_idiot_gc[:-1]

    # Calling get_indices function
    # 1st normalized Shannon index
    # 2nd Shannon index
    # 3rd Gini index

    shannon_norm, shannon, gini, shannon_pond, shannon_var = get_indices(input_file_final)

    # Open a file to save results
    salida = open(output_file, 'w')
    
    # Write headers for each column 
    salida.write('Identifier\tids_cg\treads_cg\treads_cg_norm\tGini\tShannon\tShannon_norm\tShannon_pon\tShannon_Var\tMut_sin\tMut_nosin\tid_Idiotype\n')

    # KaKs from the most representative Idiotype
    archivo_temp = make_temp_file(input_file_final)
    os.mkdir(dir_name)
    divide_groups(archivo_temp)

    for el in range(len(lista_reads_gc)):
        el_reads_gc = lista_reads_gc[el].split('\t')
        el_idiot_gc = lista_idiot_gc[el].split('\t')
        
        # normalized reads for each clonal group
        gc_norm = (float(el_reads_gc[1]) * 100) / tam_idiot_gc
        mutaciones = get_maximum(str(el_reads_gc[0]))

        salida.write(str(el_reads_gc[0]) + '\t' + str(el_idiot_gc[1]) + '\t' + str(el_reads_gc[1]) + '\t' + str(gc_norm) + '\t' + gini[el] + '\t' + shannon[el] + '\t' + shannon_norm[el] + '\t' + shannon_pond[el] + '\t' + shannon_var[el] + '\t' + mutaciones[0] + '\t' + mutaciones[1] + '\t' + mutaciones[2] + '\n')

    salida.close()

    # Remove temp files and dirs
    os.popen('rm ' + archivo_temp)
    shutil.rmtree(dir_name)
    

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='complete_table', epilog="complete_table is part of ImmunediveRsity and it is GNU software")

    parser.add_argument('-g', '--groupsfullVJ', required=True, help='groupsfullVJ created by ImmunediveRsity')
    parser.add_argument('-f', '--final-file', required=True, help='final file created by network_graph (ImmunediveRsity)')
    parser.add_argument('-o', '--output', required=True, help='output file name .txt')

    args = parser.parse_args()

    if args.groupsfullVJ and args.final_file and args.output:
        if(os.path.isdir(dir_name)):
            os.popen('rm -fr ' + dir_name)
        complete_table(args.groupsfullVJ, args.final_file, args.output)
        print("Table 'complete' DONE!!!")
