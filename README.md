** ImmunediveRsity **
=====================


Evaluation of the antibody repertoire by analyzing HTS data.
------------------------------------------------------------


** Description: **
------------------

Manipulation and processing of HTS reads to identify VDJ usage and clonal origin to gain insight of the antibody repertoire of a given organism.

** Version **

1.0.8


** Requirements: **
-------------------

GNU/Linux or MacOS with:

* R >= 3.0 [R-project](http://cran.r-project.org/),

* Python >= 2.7 or Python 3 [Python project](http://www.python.org/download/),

* Java >= 1.6 [Java download page](https://www.java.com/download/), and 

* HMMER 3.0 [HMMER archive](http://hmmer.janelia.org/software/archive)


**Dependencies: **
------------------


*R:* 

* seqinr 3.0.7 ( install.packages('seqinr') )

* vegan 2.0-9 ( install.packages('vegan') )

* gplots 2.11.0 ( install.packages('gplots') )

* stringr 0.6.2 ( install.packages('stringr') )

* Peptides 0.4 ( install.packages('Peptides') )

* ShortRead 1.16.4 [BioConductor/ShortRead](http://www.bioconductor.org/packages/2.14/bioc/html/ShortRead.html)


_All libraries will be installed/updated automatically_

*python:* 

* iGraph 0.6.5 (it is optional) [Download](http://igraph.sourceforge.net/download.html) or [use this tuto](http://igraph.wikidot.com/installing-python-igraph-on-linux) ImmunediveRsity works fine without igraph

* psutil ( easy_install psutil or pip install psutil)

* pandas (pip install pandas). For the post-processing multi-library analysis toolbox


*Other:*

* Blast 2.2.22(included)

* acacia 1.52.b0(included)

* igBlast(included)

* usearch >= 6.1.544 [Download](http://www.drive5.com/usearch/download.html)


** Download **
--------------

* For [GNU/Linux](https://bitbucket.org/ImmunediveRsity/immunediversity/downloads/ImmunediveRsity-Linux-1.0.8.tar.gz)

* For [MacOS](https://bitbucket.org/ImmunediveRsity/immunediversity/downloads/ImmunediveRsity-Darwin-1.0.8.tar.gz)

Or visit our [Downloads](https://bitbucket.org/ImmunediveRsity/immunediversity/downloads) page


** How to install **
--------------------

1- Download the tar.gz file according to your system. 

> GNU/Linux - wget https://bitbucket.org/ImmunediveRsity/immunediversity/downloads/ImmunediveRsity-Linux-1.0.8.tar.gz

> MacOS     - wget https://bitbucket.org/ImmunediveRsity/immunediversity/downloads/ImmunediveRsity-Darwin-1.0.8.tar.gz

2- untar file with

> tar -xzvf ImmunediveRsity-XXXX-X.X.X.tar.gz

3- Download [usearch >= 6.1.544](http://www.drive5.com/usearch/download.html)

4- Move usearch6.1.544_XX to ImmunediveRsity/bin.

> mv /home/user/Downloads/usearch6.1.544_XX /home/user/Downloads/ImmunediveRsity/bin/.

5- Move into ImmunediveRsity.
> cd ImmunediveRsity/

6- If you want to configure ImmunediveRsity to receive emails at the end of process, configure it using ImmunediveRsity_configure script in ImmunediveRisty/bin (only works with gmail accounts). 
Ex.
> python ImmunediveRsity_configure -e user@gmail.com -p top_secret_password

8- Install ImmunediveRsity

> ./install.sh

PLEASE use _./install.sh_ instead _sh install.sh_


** How to use **
----------------

Ex.
> ImmunediveRsity -i intput_dir -o output_dir [-c cores_to_use -n email@gmail.com -r] 

or

> ImmunediveRsity -h


** Parameters **
----------------

** -i ** : Full path to your fastq files.

** -o ** : Full output path.

** -c ** : Number of cores to use. (Default: 1)

** -n ** : Notify when ImmunediveRsity finished.

** -l ** : Minimum read length to consider. (Default: 200)

** -s ** : Species to work, can be *human* or *mouse*. (Default: human)

** -d ** : Minimum identity. Used by [usearch](http://drive5.com/usearch/manual6/accept_options.html). (Default: 0.92) 

** -q ** : Query fract. Fraction of the query sequence that is aligned, in the range 0.0 to 1.0. Used by [usearch](http://drive5.com/usearch/manual6/accept_options.html) (Default: 0.97)

** -t ** : Target fract. Fraction of the target sequence that is aligned, in the range 0.0 to 1.0. Used by [usearch](http://drive5.com/usearch/manual6/accept_options.html) (Default: 0.97)

** -a ** : Show alleles in read identifier. (Boolean flag)

** -r ** : Remove introns - Create directory whit all intronic sequences -(only for human). (Bollean flag) 

** -cc ** : Clonotype minimal frequency to cut. (Default: 6)

** -ic ** : lineage minimal frequency to cut. (Default: 6)

** -m ** : Make a network graph. (Boolean flag)

** -p ** : Run  [acacia](ftp://ftp3.ie.freebsd.org/pub/sourceforge/a/project/ac/acaciaerrorcorr/acacia-v1.52/acacia_documentation_version_1.52.pdf) to fix homopolymers. (Boolean flag) 

** -mq ** : Mean quality, Filter by mean quality. (Default: 28)

** -v ** : Show ImmunediveRsity version.

** -h ** : Show help.

_Boolean flag:_ if parameter is present equals true if not present equals false.


** Library name **
------------------

Libraries names must be without points(.) or blankspaces ( ).

we recommend short names with underscores or dashes instead of points or blankspaces


** Output **
------------

** Dirs **

* Intron_Exon_Sorting : Directory with all intronic sequences found.

* WellSupportedCGs : This directory will contain files only with Well Supported Clonal groups (clonotypes with more reads than minimal frequency cut), no matters if they contain lineages with low frequency. Here, the number of lineages is greater or equal to the number of clonotypes.

* WellSupportedIs : This directory will contain files only with Well Supported Lineages (lineages with more reads than minimal frequency cut), no matters if this leaves clonotypes with no associated lineages, this way the number of clonotypes could be greater to the number of lineages.

** ID read **

The ID include library's name, V segment, J segment, the number of Clonotype and the number of the lineage in that Clonotype. The number of the Clonotype and lineages starts in zero "0" .Example:

HAVFR-i01.IGHV1-69.IGHJ5.10.0. This read belongs to the library "HAVFR-i01", V segment "IGHV1-69", J segment "IGHJ5", Clonotype "10" and lineage "0".


** Fasta files **

* Alleles_onlyVDJ.fa : FASTA file with the consensus of each lineage. 

* CDR3.fa : FASTA file include CDR3 of each read.

* LibName.fna : FASTA file after Accacia process.

* consensusVJ.fa : FASTA file with the consensus of each Clonotype.

* consensusfullVJ.fa : FASTA file with the consensus of each lineage. 

* onlyVDJ.fa : FASTA file with only the VDJ segment.

* reads.fa : FASTA file afther the get CDR3 process. Include only VDJ segment.

* WellSupportedCGs/onlyV.fa : FASTA file with only V segment from the reads of Clonotype with more than 6 reads.


** Fastq files **

* libName.fastq : FASTQ file with the row sequences

* libName_VDJ.fastq : FASTQ file with onlyVDJ segment.

* libName_filtered.fastq : FASTQ file after filtered the sequences for quality (Default:28), size (Default: 200bp) and VDJ assignment. 

* reads.fastq : FASTQ file after filtered the sequences for quality (Default:28), size (Default: 200bp), VDJ assignment and cut names. This file is the input for Acaccia.


** Graphics **

* AA_freqs_libName.eps : Proportion of each aminoacid in the CDR3 consensus vs relative frequency

* AA_proportion_l17_libName.eps : Aminoacid distribution of 17bp CDR3 consensus.

* CDR3_length_plot_libName.eps : Histogram of the CDR3 and CDR3 consensus lengths.

* freqs_VDJ_libName.pes :VDJ segment distribution from all the library vs relative frequency.

* libName_rarefaction_curve_VJ.eps : Rarefaction curve of the total of Clonotype in the library.

* libName_rarefaction_curve_idiotype.eps : Rarefaction curve of the total of lineages in the library.

* Heatmap_libName.eps : Heatmap of the reagments of V-J segments and relative frequency.

* Mean_quality.pdf : Histograms with the mean of quality with and without filters.

* Reads_Cloud_plot_libName.eps : VDJ reagments representation. The size of each point is proportional to its size in the library and the color means 

* WellSupportedCGs/network_graph.svg : Network graphical representacion of the Clonotype with their lineages. The color goes from blue to red and is proportional at number of non-synonyms mutations. 


** files **

* Alleles_consensusfullVDJ.txt : List of reads with the VDJ assignment with alleles 

* libName_all_tags_stats : Acacia stats.

* VDJ_Alleles_idiotype.txt : List of the VDJ and alleles assignment and the original name of the read.

* VDJs.txt : List of the VDJ assignment and the original name of the read.

* config.out : Acacia configuration file.

* groupsVJ.txt : This table include the list of the Clonotype for each read.

* groupsfullVJ.txt : This table include the list of the lineages for each read.

* log.out : Log file 

* WellSupportedCGs/indices_table.txt : This table include Clonotype ID, absolute frequency of lineages, absolute frequency of reads, relative frequency of reads, Clonotype Gini index, Clonotype Shannon index, Clonotype Shannon normalized, Clonotype Shannon weighted, Shannon variance, number of synonyms and non-synonyms mutation, ID majority ID. 

* WellSuppoprtedCGs/kaks.txt : Table with the number of synonyms and non-synonyms mutation

* WellSupportedCGs/to_graph.txt : This file es used to make the network graph, include all read identifiers with its synonyms and non-synonyms mutations.

* WellSupportedCGs/to_graph-final.txt : This file is used to make indices table, include each clonotype with all its lineages and synonyms and non-synonyms mutations.


** Post-Processing multi-Library Analysis Toolbox (PPLAT) **
------------------------------------------------------------

** Subset **

ImmunediveRsity_subset is a tool designed to make subsets of reads or clonotypes 

for make a subset of reads

> ImmunediveRsity_subset -p /path/to/libraries -n NUMBER_OF_READS/CLONAL_GROUPS [-t CORES/THREADS  -m -nr ]

and, to make a subset of clonotypes

> ImmunediveRsity_subset -c -p /path/to/libraries -n NUMBER_OF_READS/CLONAL_GROUPS [-t CORES/THREADS  -m -nr ]

For more information use:

> ImmunediveRsity_subset -h 


** Shared CDR3 finder **

find_CDR3 is a tool designed to find all shared CDR3 between all libraries. It uses indices_table made for ImmunediveRsity (in WellSupportedCGs directory) to get, compare and finally find all shared CDR3.

> /opt/ImmunediveRsity/bin/find_CDR3 -p /path/to/libraries -s WellSupportedCGs > stats.log


** Network graph with any index **

To make a network graph whit any index from 'indices table' to find patterns.
We provides a script to make them.

> /opt/ImmunediveRsity/ng_index


** For Illumina users **
------------------------

We provides a script [imm-illumina](https://bitbucket.org/ImmunediveRsity/imm-illumina) for preprocessing Illumina data sets.


** Authors **
-------------

** Department of Immunology, Research Center for Infectious Diseases at the National Institute of Public Health
(INSP-CISEI). Cuernavaca, Morelos. Mexico. **


* Jesus Martinez Barnetche

* Juan Tellez Soza

* Ernestina Godoy Lozano 

* Andres Aguilar Salgado

* Bernardo Cortina Ceballos

* Hugo Samano Sanchez

* Guillermo Romero Tecua


** Winter Genomics, D.F., Mexico. **

* Martin Del Castillo Velasco Herrera

* Carlos Vargas Chavez


** Maintainers **
-----------------

* Andres Aguilar - andresyoshimar[at]gmail[dot]com

* Hugo Samano - hsamano[at]lcg[dot]unam[dot]mx


** Citation **
-----------

>[Reconstructing and mining the B cell repertoire with ImmunediveRsity](http://www.tandfonline.com/doi/abs/10.1080/19420862.2015.1026502)

>Bernardo Cortina-Ceballos , Elizabeth Ernestina Godoy-Lozano , Hugo Sámano-Sánchez , Andrés Aguilar-Salgado , Martín Del Castillo Velasco-Herrera , Carlos Vargas-Chávez , Daniel Velázquez-Ramírez , Guillermo Romero , José Moreno , Juan Téllez-Sosa , Jesús Martínez-Barnetche

>mAbs

>Vol. 7, Iss. 3, 2015


** License: **
--------------

** [GPL version 2 or newer](https://www.gnu.org/licenses/gpl-2.0.html) **

