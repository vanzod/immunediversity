#!/bin/bash

#  install.sh
#  
#  Copyright 2013-2014 Andres Aguilar <andresyoshimar@gmail.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

###################################################################
#: Title          : install.sh
#: Date           : 03-01-2014
#: Author         : Andres Aguilar <andresyoshimar@gmail.com>
#: Version        : 0.2.4
#: Description    : ImmunediveRsity installer
#: Options        : none
###################################################################

title='ImmunediveRsity'
subtitle='Evaluation of the antibody repertoire by analyzing HTS data'
ast='*****************************************************************'
deps=1

DEPS=0
WDIR=$(pwd)

verify_system() {
    systemName=$(uname)
    
    if [ "$systemName" = "Darwin" ]; then
	systemName="MacOS"
    elif [ "$systemName" = "Linux" ]; then
	systemName="Linux"
    else
	systemName="Unkown"
    fi

    echo "$systemName"
}


log_msg() {
    RED=$(tput setaf 1)
    GREEN=$(tput setaf 2)
    NORMAL=$(tput sgr0)

    MSG="$1"
    STATUS="$2"

    if [ "$STATUS" == 0 ] ;  then
	STATUSCOLOR="$GREEN[OK]$NORMAL"
	STATUS2="[OK]"
    else
	STATUSCOLOR="$RED[ERROR]$NORMAL"
	STATUS2="[ERROR]"
    fi

    let COL=$(tput cols)-${#MSG}+${#STATUSCOLOR}-${#STATUS2}

    echo -n $MSG
    printf "%${COL}s\n"  "$STATUSCOLOR"
}

echo_c() {
    w=$(stty size | cut -d" " -f2)
    l=${#1}
               
    printf "%"$((l+(w-l)/2))"s\n" "$1"
}

remove_files() {
    echo -e "\n\n\nABORTING....."
    echo "Removing files"

    # remove ImmunediveRsity directory
    if [ -d /opt/ImmunediveRsity ] ; then
	sudo rm -fr /opt/ImmunediveRsity 
    fi
    
    # remove symbolic links
    if [ -L /usr/bin/ImmunediveRsity ] ; then
	sudo rm /usr/bin/ImmunediveRsity
    fi

    if [ -L /usr/bin/ImmunediveRsity_notifier ] ; then
	sudo rm /usr/bin/ImmunediveRsity_notifier
    fi

    if [ -L /usr/bin/blastall ] ; then
	sudo rm /usr/bin/blastall 
    fi

    echo "DONE"

    exit 1
}

verify_error() {
    msg="$1"
    error="$2"

    if [ "$error" == 1 ] ; then
	log_msg "Error: $msg" "$error"
	remove_files
    else
	log_msg "$msg" "$error"
    fi
}

wellcome() {
    clear

    echo_c "$ast"
    echo_c "$title"
    echo_c "$subtitle"
    echo_c "$ast"
}

verify_prev_installation() {
    # verifying if immunediversity dir exist or if ImmunediveRsity and ImmunediveRsity_notifier exist
    echo "Verify prev installation..."
    
    if [ -d $PREFIX/ImmunediveRsity ] ; then
	log_msg "ImmunediveRsity" 1
	echo "Immunodiversity is already installed, please uninstall to continue..."

	exit 1
    else
	log_msg "ImmunediveRsity" 0
    fi
}

verify_dependencies() {
    # verify if all dependencies are already installed
    echo -e '\nVerifying dependencies...\n'

    error1=0

    # search python
    python=$(whereis python)
    if [ "$python" == "" ] ; then
	log_msg 'Python' 1
	echo -e '\nPython is not installed\nAborting...'
	exit 1
    else
	log_msg 'Python' 0
    fi
    
    # checking Igraph
    python $WDIR/installer/check_igraph.py
    error=$?

    if [ "$error" == 0 ]; then
	log_msg 'iGraph(python)' 0
    else
	log_msg 'iGraph(python)' 1
	echo -e "\npython-igraph is not installed!\n"
	echo "Network graphs will not generated"
    fi

    # checking psutil
    python $WDIR/installer/check_psutil.py
    error=$?

    if [ "$error" == 0 ]; then
	log_msg 'psutil(python)' 0
    else
	log_msg 'psutil(python)' 1
	echo -e "\npython-psutil is not installed!\nAborting..."
	exit 1
    fi


    # search R
    rbin=$(whereis R)
    if [ "$rbin" == "" ] ; then
	log_msg "R" 1
	echo -e '\nR is not installed\nAborting...'
	exit 1
    else    
	if [ "$sys" == "MacOS" ] ; then
	    log_msg 'R' 0
	fi
    fi

    # checking R libraries
    R -q -e "source('installer/dependency_checker.R')" || ! echo 'Error: R dependencies not found!'
    error=$?
	    
    if [ "$error" == 0 ] ; then
	log_msg "R libraries" 0
	deps=0
    else
	log_msg "R libraries" 1
	echo -e "\nR libraries are not installed!\n"
	echo -e "Will be installed later."
	DEPS=1
	#sudo R -q -e "source('installer/install_deps.R')"
    fi

    # Search hmmer
    hmmer=$(whereis hmmsearch)
    # hmmer in MacOS - /usr/local/bin/hmmsearch by default
    sys=$(verify_system)

    if [ "$sys" == "MacOS" ] ; then 
	if [ -x /usr/local/bin/hmmsearch ] ; then 
	    hmmer="exist"
	fi
    fi

    if [ "$hmmer" == "" ] ; then
	log_msg "HMMR" 1
	echo -e "\nHMMR is not installed!\nAborting..."
	exit 1
    else
	log_msg  "HMMR" 0
    fi
}

install() {
    
    ################
    # install deps #
    ################

    if [ "$DEPS" -eq 1 ]; then
	R -q -e "source('installer/install_deps.R')"
    fi

    # Create ImmunediveRsity directory
    mkdir $PREFIX/ImmunediveRsity
    verify_error "Creating ImmunediveRsity directory" "$?"
	    
    ################
    # moving files #
    ################

    cp uninstall.sh $PREFIX/ImmunediveRsity/.
    verify_error "Moving uninstall.sh" "$?"
	    
    mkdir $PREFIX/ImmunediveRsity/uninstaller
    verify_error "Creating $PREFIX/ImmunediveRsity/uninstaller" "$?"

    cp uninstaller/* $PREFIX/ImmunediveRsity/uninstaller/.
    verify_error "Moving uninstaller scripts" "$?"

    cp COPYING $PREFIX/ImmunediveRsity/.
    verify_error "Moving COPYING to $PREFIX/ImmunediveRsity" "$?"

    cp README.md $PREFIX/ImmunediveRsity/.
    verify_error "Moving README.md to $PREFIX/ImmunediveRsity" "$?"

    # create bin directory
    mkdir $PREFIX/ImmunediveRsity/bin 
    verify_error "Creating $PREFIX/ImmunediveRsity/bin directory" "$?"

    cp bin/ImmunediveRsity $PREFIX/ImmunediveRsity/bin/.
    verify_error "Moving bin/ImmunediveRsity" "$?"
	    
    cp bin/ImmunediveRsity_notifier $PREFIX/ImmunediveRsity/bin/.
    verify_error "Moving bin/ImmunediveRsity_notifier" "$?"

    cp bin/ImmunediveRsity_subset $PREFIX/ImmunediveRsity/bin/.
    verify_error "Moving bin/ImmunediveRsity_subset" "$?"

    cp bin/acacia-1.52.b0.jar $PREFIX/ImmunediveRsity/bin/.
    verify_error "Moving /bin/acacia" "$?"

    cp bin/cutVDJ.pl $PREFIX/ImmunediveRsity/bin/.
    verify_error "Moving bin/cutVDJ.pl" "$?"

    cp bin/parseKaks $PREFIX/ImmunediveRsity/bin/.
    verify_error "Moving bin/parseKaks" "$?"

    cp bin/network_graph $PREFIX/ImmunediveRsity/bin/.
    verify_error "Moving bin/network_graph" "$?"

    cp bin/complete_table $PREFIX/ImmunediveRsity/bin/.
    verify_error "Moving bin/complete_table" "$?"

    cp bin/div_table $PREFIX/ImmunediveRsity/bin/.
    verify_error "Moving bin/div_table" "$?"

    cp bin/usearc* $PREFIX/ImmunediveRsity/bin/usearch
    verify_error "Moving bin/usearch" "$?"

    #cp bin/parseKaks_subset $PREFIX/ImmunediveRsity/bin/.
    #verify_error "Moving bin/parseKaks_subset" "$?"

    # Creating bin/blast directories
    mkdir $PREFIX/ImmunediveRsity/bin/blast-2.2.22 
    verify_error "Creating $PREFIX/ImmunediveRsity/bin/blast-2.2.22 directory" "$?"

    mkdir $PREFIX/ImmunediveRsity/bin/blast-2.2.22/bin
    verify_error "Creating $PREFIX/ImmunediveRsity/bin/blast-2.2.22/bin directory" "$?"

    mkdir $PREFIX/ImmunediveRsity/bin/blast-2.2.22/data 
    verify_error "Creating $PREFIX/ImmunediveRsity/bin/blast-2.2.22/data directory" "$?"

    cp bin/blast-2.2.22/bin/* $PREFIX/ImmunediveRsity/bin/blast-2.2.22/bin/.
    verify_error "Moving blast binaries" "$?"

    cp bin/blast-2.2.22/data/* $PREFIX/ImmunediveRsity/bin/blast-2.2.22/data/.
    verify_error "Moving blast data" "$?"

    # creating ImmunediveRsity/data directory
    mkdir $PREFIX/ImmunediveRsity/data
    verify_error "Creating $PREFIX/ImmunediveRsity/data" "$?"

    cp data/CDR3_IGHV_human.hmm $PREFIX/ImmunediveRsity/data/.
    verify_error "Moving data/CDR3_IGHV_human.hmm" "$?"

    cp data/CDR3_mouse.hmm $PREFIX/ImmunediveRsity/data/.
    verify_error "Moving data/CDR3_IGHV_mouse.hmm" "$?"

    cp data/blastfirsthit_q.pl $PREFIX/ImmunediveRsity/data/.
    verify_error "Moving data/blastfirtshit_q.pl" "$?"

    cp data/gene_coordinates.txt $PREFIX/ImmunediveRsity/data/.
    verify_error "Moving data/gene_coordinates.txt" "$?"

    # Creating data/LocusIgH_db
    mkdir $PREFIX/ImmunediveRsity/data/LocusIgH_db
    verify_error "Creating $PREFIX/ImmunediveRsity/data/LocusIgH_db" "$?"

    cp data/LocusIgH_db/* $PREFIX/ImmunediveRsity/data/LocusIgH_db/.
    verify_error "Moving data/LocusIgH_db files" "$?"

    # creating data/IGBLAST
    mkdir $PREFIX/ImmunediveRsity/data/IGBLAST
    verify_error "Creating $PREFIX/ImmunediveRsity/data/IGBLAST directory" "$?"

    cp data/IGBLAST/ig* $PREFIX/ImmunediveRsity/data/IGBLAST/.
    verify_error "Moving igblast binaries" "$?"
	    
    cp data/IGBLAST/myseq $PREFIX/ImmunediveRsity/data/IGBLAST/.
    verify_error "moving IGBLAST/myseq" "$?"

    # creating data/IGBLAST/database
    mkdir $PREFIX/ImmunediveRsity/data/IGBLAST/database
    verify_error "Creating $PREFIX/ImmunediveRsity/data/IGBLAST/database" "$?"

    cp data/IGBLAST/database/* $PREFIX/ImmunediveRsity/data/IGBLAST/database/.
    verify_error "Moving IGBLAST/database" "$?"

    # creating data/IGBLAST/functional_db
    mkdir $PREFIX/ImmunediveRsity/data/IGBLAST/functional_db 
    verify_error "Creating $PREFIX/ImmunediveRsity/data/IGBLAST/functional_db" "$?"

    cp data/IGBLAST/functional_db/* $PREFIX/ImmunediveRsity/data/IGBLAST/functional_db/.
    verify_error "Moving IGBLAST/functional_db" "$?"

    # creating IGBLAST/internal_data
    mkdir $PREFIX/ImmunediveRsity/data/IGBLAST/internal_data
    verify_error "Creating $PREFIX/ImmunediveRsity/data/IGBLAST/internal_data" "$?"
	    
    mkdir $PREFIX/ImmunediveRsity/data/IGBLAST/internal_data/human
    verify_error "Creating $PREFIX/ImmunediveRsity/data/IGBLAST/internal_data/human" "$?"

    cp data/IGBLAST/internal_data/human/* $PREFIX/ImmunediveRsity/data/IGBLAST/internal_data/human/.
    verify_error "Moving internal_data/human" "$?"

    mkdir $PREFIX/ImmunediveRsity/data/IGBLAST/internal_data/mouse
    verify_error "Creating $PREFIX/ImmunediveRsity/data/IGBLAST/internal_data/mouse" "$?"

    cp data/IGBLAST/internal_data/mouse/* $PREFIX/ImmunediveRsity/data/IGBLAST/internal_data/mouse/.
    verify_error "Moving internal_data/mouse" "$?"

    # creating IGBLAST/optional_file
    mkdir $PREFIX/ImmunediveRsity/data/IGBLAST/optional_file
    verify_error "Creating $PREFIX/ImmunediveRsity/data/IGBLAST/optional_file" "$?"

    cp data/IGBLAST/optional_file/* $PREFIX/ImmunediveRsity/data/IGBLAST/optional_file/.
    verify_error "Moving IGBLAST/optional_file" "$?"

    # Creating ImmunediveRsity/ImmunediveRsity directories
    mkdir $PREFIX/ImmunediveRsity/ImmunediveRsity
    verify_error "Creating $PREFIX/ImmunediveRsity/ImmunediveRsity" "$?"

    cp ImmunediveRsity/DESCRIPTION $PREFIX/ImmunediveRsity/ImmunediveRsity/.
    verify_error "Moving ImmunediveRsity/DESCRIPTION" "$?"

    cp ImmunediveRsity/NAMESPACE $PREFIX/ImmunediveRsity/ImmunediveRsity/.
    verify_error "Moving ImmunediveRsity/NAMESPACE" "$?"

    # Creating ImmunediveRsity R directory
    mkdir $PREFIX/ImmunediveRsity/ImmunediveRsity/R 
    verify_error "Creating $PREFIX/ImmunediveRsity/ImmunediveRsity/R directory" "$?"

    cp ImmunediveRsity/R/* $PREFIX/ImmunediveRsity/ImmunediveRsity/R/.
    verify_error "Moving R libraries" "$?"
	    
    # Creating ImmunediveRsity/extdata
    mkdir $PREFIX/ImmunediveRsity/ImmunediveRsity/extdata
    verify_error "Creating $PREFIX/ImmunediveRsity/ImmunediveRsity/extdata" "$?"
	    
    cp ImmunediveRsity/extdata/* $PREFIX/ImmunediveRsity/ImmunediveRsity/extdata
    verify_error "Moving ImmunediveRsity/extdata" "$?"

    # Creating ImmunediveRsity/extras
    mkdir $PREFIX/ImmunediveRsity/ImmunediveRsity/extras
    verify_error "Creating $PREFIX/ImmunediveRsity/ImmunediveRsity/extras" "$?"

    cp ImmunediveRsity/extras/* $PREFIX/ImmunediveRsity/ImmunediveRsity/extras/.
    verify_error "Moving ImmunediveRsity/extras" "$?"

    # Creating ImmunediveRsity/man
    mkdir $PREFIX/ImmunediveRsity/ImmunediveRsity/man
    verify_error "Creating $PREFIX/ImmunediveRsity/ImmunediveRsity/man" "$?"

    cp ImmunediveRsity/man/* $PREFIX/ImmunediveRsity/ImmunediveRsity/man/.
    verify_error "Moving ImmunediveRsity/man" "$?"

    # Goto ImmunediveRsity directory
    cd $PREFIX/ImmunediveRsity

    # Add executable permission
    chmod +x $PREFIX/ImmunediveRsity/bin/ImmunediveRsity*
    verify_error "Setting executable permission to ImmunediveRsity" "$?"

    chmod +x $PREFIX/ImmunediveRsity/bin/*
    verify_error "Setting executable permission to ImmunediveRsity/bin" "$?"

    chmod +x $PREFIX/ImmunediveRsity/bin/blast-2.2.22/bin/*
    verify_error "Setting executable permission to blast-2.2.22" "$?"

    chmod +x $PREFIX/ImmunediveRsity/data/IGBLAST/igblastn 
    verify_error "Setting executable permission to igblastn" "$?"

    chmod +x $PREFIX/ImmunediveRsity/data/IGBLAST/igblastp 
    verify_error "Setting executable permission to igblastp" "$?"

    ########################
    # adding files to path #
    ########################

    # creating symlinks to /usr/bin
    tput bold
    echo
    read -p 'Do you want to create symlinks in /usr/bin? (Requires root privileges) [y/n] ' -n 1 -r
    echo
    until [[ $REPLY == 'y' || $REPLY == 'n' ]]; do
	read -p 'Please answer "y" for yes or "n" for no: '  -n 1 -r
	echo
    done

    if [[ $REPLY == 'y' ]]; then
	if [[ $EUID -ne 0 ]]; then
	    echo
	    echo "Liar! You are not root. Skipping symlink creation"
	    REPLY=n   # So that it shows the following PATH export message
	else
	    ln -s $PREFIX/ImmunediveRsity/bin/ImmunediveRsity /usr/bin/ImmunediveRsity
	    verify_error "Creating symbolic link to /usr/bin/ImmunediveRsity" "$?"
	    
	    ln -s $PREFIX/ImmunediveRsity/bin/ImmunediveRsity_notifier /usr/bin/ImmunediveRsity_notifier
	    verify_error "Creating symbolic link to /usr/bin/ImmunediveRsity_notifier" "$?"
	    
	    ln -s $PREFIX/ImmunediveRsity/bin/ImmunediveRsity_subset /usr/bin/ImmunediveRsity_subset
	    verify_error "Creating symbolic link to /usr/bin/ImmunediveRsity_subset" "$?"
	    
	    ln -s $PREFIX/ImmunediveRsity/bin/blast-2.2.22/bin/blastall /usr/bin/blastall
	    verify_error "Creating symbolic link to /usr/bin/blastall" "$?" 
	fi
    fi

    if [[ $REPLY == 'n' ]]; then
	tput setaf 1
	cat << EOF

Don't forget to add the binary install paths to your PATH environment variable with:

     export PATH=$PREFIX/ImmunediveRsity/bin:$PREFIX/ImmunediveRsity/bin/blast-2.2.22/bin:\$PATH

EOF
    fi

    tput sgr0

    ###############
    # configuring #
    ###############

    sys=$(verify_system)

    if [ "$sys" = "Linux" ]; then
	sed -i "s+\$/ = \"\# IGBLASTN 2.2.27\+\";+\$/ = \"\# IGBLASTN 2.2.26\+\";+g" $PREFIX/ImmunediveRsity/data/IGBLAST/igparse.pl
	verify_error "Configuring igparse.pl" "$?"

	sed -i "s/-auxiliary_data/-auxilary_data/g" $PREFIX/ImmunediveRsity/ImmunediveRsity/R/runigblast.R
	verify_error "Configuring runigblast" "$?"
    fi
	    
    ########################
    # Installing R library #
    ########################

    echo ""
    R CMD INSTALL ./ImmunediveRsity
    echo ""
    verify_error "Installing R libraries" "$?"
}

if [ "$#" -ne 1 ]; then
    echo "Usage: ./install.sh <install_path>"
    exit 1
else
    PREFIX=$1
fi

wellcome
verify_prev_installation
verify_dependencies
install
