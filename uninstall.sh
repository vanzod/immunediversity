#!/bin/bash

#  uninstall.sh
#  
#  Copyright 2013-2014 Andres Aguilar <andresyoshimar@gmail.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

###################################################################
#: Title          : uninstall.sh
#: Date           : 09-01-2014
#: Author         : Andres Aguilar <andresyoshimar@gmail.com>
#: Version        : 0.1.6
#: Description    : ImmunediveRsity uninstaller
#: Options        : none
###################################################################


title='ImmunediveRsity'
subtitle='Evaluation of the antibody repertoire by analyzing HTS data'
ast='*****************************************************************'

log_msg() {
    RED=$(tput setaf 1)
    GREEN=$(tput setaf 2)
    NORMAL=$(tput sgr0)

    MSG="$1"
    STATUS="$2"

    if [ "$STATUS" == 0 ] ; then
	STATUSCOLOR="$GREEN[OK]$NORMAL"
	STATUS2="[OK]"
    else
	STATUSCOLOR="$RED[ERROR]$NORMAL"
	STATUS2="[ERROR]"
    fi

    let COL=$(tput cols)-${#MSG}+${#STATUSCOLOR}-${#STATUS2}

    echo -n $MSG
    printf "%${COL}s\n"  "$STATUSCOLOR"
}

echo_c() {
    w=$(stty size | cut -d" " -f2)
    l=${#1}

    printf "%"$((l+(w-l)/2))"s\n" "$1"
}

remove_files() {

    # remove ImmunediveRsity directory
    if [ -d /opt/ImmunediveRsity ] ; then
	sudo rm -fr /opt/ImmunediveRsity 
    fi
    
    # remove symbolic links
    if [ -L /usr/bin/ImmunediveRsity ] ; then
	sudo rm /usr/bin/ImmunediveRsity
    fi

    if [ -L /usr/bin/ImmunediveRsity_notifier ] ; then
	sudo rm /usr/bin/ImmunediveRsity_notifier
    fi

    if [ -L /usr/bin/ImmunediveRsity_subset ] ; then
	sudo rm /usr/bin/ImmunediveRsity_subset
	fi

    if [ -L /usr/bin/blastall ] ; then
	sudo rm /usr/bin/blastall 
    fi

    exit 1
}

verify_error() {
    msg="$1"
    error="$2"

    if [ "$error" == 1 ] ; then
	log_msg "Error: $msg" "$error"
	remove_files
    else
	log_msg "$msg" "$error"
    fi
}

wellcome() {
    clear

    echo_c "$ast"
    echo_c "$title"
    echo_c "$subtitle"
    echo_c "$ast"
}

verify_installation() {
    # verifying if immunediversity ins installed 
    echo "Verify prev installation..."

    if [ ! -d /opt/ImmunediveRsity ] ; then
	verify_error "Find ImmunediveRsity installation" 1
    else
        verify_error "find ImmunediveRsity installation" 0
    fi
}

uninstall() {
    echo 'INTRO TO CONTINUE...'
    read
 
    ## root access
    case $y in
	etch )
	    echo 'root password (su)'
	    su
	    echo 'Starting uninstallation process' 
	    ;;
	* )
	    echo 'root password (sudo)'
	    sudo echo 'Starting uninstallation process'
	    ;;
    esac
    
    echo ""
    echo ""
    
    case $y in 
	etch )
	    # Removing symbolic links
	    if [ -L /usr/bin/ImmunediveRsity ] ; then
		rm /usr/bin/ImmunediveRsity
		verify_error "Removing ImmunediveRsity symlink" "$?"
	    fi

	    if [ -L /usr/bin/ImmunediveRsity_notifier ] ; then
		rm /usr/bin/ImmunediveRsity_notifier
		verify_error "Removing ImmunediveRsity_notifier symlink" "$?"
	    fi

	    if [ -L /usr/bin/ImmunediveRsity_subset ] ; then
		rm /usr/bin/ImmunediveRsity_subset
		verify_error "Removing ImmunediveRsity_subset symlink" "$?"
	    fi

	    if [ -L /usr/bin/blastall ] ; then
		rm /usr/bin/blastall
		verify_error "Removing blastall symlink" "$?"
	    fi

	    if [ -d /opt/ImmunediveRsity ] ; then
		cd /opt/ImmunediveRsity
	
		# Uninstalling R libraries
		R -q -e "source('uninstaller/uninstall.R')"
		verify_error "Removing ImmunediveRsity R library" "$?"

		# cleanning data
		cd ~/
		rm -fr /opt/ImmunediveRsity 
		verify_error "Removing ImmunediveRsity directory" "$?"
	    fi
    	    ;;
	* )
	    # Removing symbolic links
	    if [ -L /usr/bin/ImmunediveRsity ] ; then
		sudo rm /usr/bin/ImmunediveRsity
		verify_error "Removing ImmunediveRsity symlink" "$?"
	    fi

	    if [ -L /usr/bin/ImmunediveRsity_notifier ] ; then
		sudo rm /usr/bin/ImmunediveRsity_notifier
		verify_error "Removing ImmunediveRsity_notifier symlink" "$?"
	    fi

	    if [ -L /usr/bin/ImmunediveRsity_subset ] ; then
		sudo rm /usr/bin/ImmunediveRsity_subset
		verify_error "Removing ImmunediveRsity_subset symlink" "$?"
	    fi

	    if [ -L /usr/bin/blastall ] ; then
		sudo rm /usr/bin/blastall
		verify_error "Removing blastall symlink" "$?"
	    fi

	    if [ -d /opt/ImmunediveRsity ] ; then
		cd /opt/ImmunediveRsity
	
		# Uninstalling R libraries
		sudo R -q -e "source('uninstaller/uninstall.R')"
		verify_error "Removing ImmunediveRsity R library" "$?"

		# cleanning data
		cd ~/
		sudo rm -fr /opt/ImmunediveRsity 
		verify_error "Removing ImmunediveRsity directory" "$?"
	    fi
	    ;;
	esac
}


wellcome
verify_installation
uninstall
